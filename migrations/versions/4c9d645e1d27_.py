"""empty message

Revision ID: 4c9d645e1d27
Revises: 58f85e33c48e
Create Date: 2023-12-14 08:01:08.085946

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "4c9d645e1d27"
down_revision = "58f85e33c48e"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("ignore_require_report", sa.Boolean(), nullable=True)
        )


def downgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.drop_column("ignore_require_report")
