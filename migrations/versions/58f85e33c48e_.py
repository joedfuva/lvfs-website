# Custom template
"""empty message

Revision ID: 58f85e33c48e
Revises: dc01a86d93e3
Create Date: 2023-11-14 12:33:05.266337

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "58f85e33c48e"
down_revision = "dc01a86d93e3"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("auth_required_for_metadata", sa.Boolean(), nullable=True)
        )


def downgrade():
    with op.batch_alter_table("vendors", schema=None) as batch_op:
        batch_op.drop_column("auth_required_for_metadata")
