"""empty message

Revision ID: 587aebb3c40f
Revises: 87e6454c1003
Create Date: 2023-05-19 15:55:36.366230

"""
from alembic import op
import sqlalchemy as sa


revision = "587aebb3c40f"
down_revision = "87e6454c1003"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("mirrors", schema=None) as batch_op:
        batch_op.add_column(sa.Column("metadata_basename", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("mirrors", schema=None) as batch_op:
        batch_op.drop_column("metadata_basename")
