"""empty message

Revision ID: 6785bd282c53
Revises: b10404a4b72f
Create Date: 2022-12-20 17:31:32.696032

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "6785bd282c53"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("protocol", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column("allow_device_category", sa.Boolean(), nullable=True)
        )


def downgrade():
    with op.batch_alter_table("protocol", schema=None) as batch_op:
        batch_op.drop_column("allow_device_category")
