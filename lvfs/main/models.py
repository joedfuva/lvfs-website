#!/usr/bin/python3
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import datetime

from sqlalchemy import Column, Integer, Text, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db


class Event(db.Model):  # type: ignore
    __tablename__ = "event_log"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False, index=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    address = Column("addr", Text, nullable=False)
    message = Column(Text, default=None)
    is_important = Column(Boolean, default=False)
    request = Column(Text, default=None)
    container_id = Column(Text, default=None)

    vendor = relationship("Vendor", foreign_keys=[vendor_id], back_populates="events")
    user = relationship("User", foreign_keys=[user_id], back_populates="events")

    def __repr__(self) -> str:
        return f"Event({self.message})"
