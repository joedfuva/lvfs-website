#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from lvfs import create_app

if __name__ == "__main__":
    from flask import Flask

    server = Flask(__name__)
    server.wsgi_app = create_app()
    server.run(host=server.wsgi_app.config["IP"], port=server.wsgi_app.config["PORT"])
else:
    app = create_app()
